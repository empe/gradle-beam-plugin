# Gradle BEAM Plugin
Simple utility that aids [Apache BEAM](https://beam.apache.org/) pipeline development with gradle. 

The task takes care for Java SDK and proper runner on classpath, however if you plan to use any additional *beam* components
, e.g. IO's you should load them as a standard dependency. 
