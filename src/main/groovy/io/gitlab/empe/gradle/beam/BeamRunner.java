package io.gitlab.empe.gradle.beam;


public enum BeamRunner {
    DIRECT("direct", "beam-runners-direct-java"),
    DATAFLOW("DataflowRunner", "beam-runners-google-cloud-dataflow-java"),
    SPARK("SparkRunner", "beam-runners-spark"),
    APEX("ApexRunner", "beam-runners-apex"),
    FLINK("FlinkRunner", "beam-runners-flink_2.10");


    BeamRunner(String name, String artifact){
        this.name = name;
        this.artifact = artifact;
    }

    public final String name;
    public final String artifact;

}
