package io.gitlab.empe.gradle.beam

class BeamExtension {

    private static final BeamRunner DEFAULT_RUNNER = BeamRunner.DIRECT
    private static final String DEFAULT_SDK_VERSION = "2.2.0"

    static final String NAME = "beam"

    BeamRunner runner = DEFAULT_RUNNER
    String version = DEFAULT_SDK_VERSION
    String main
    Map parameters = [:]
    Map environment = [:]
    List jvmArgs = []
}
