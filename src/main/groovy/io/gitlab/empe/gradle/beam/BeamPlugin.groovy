package io.gitlab.empe.gradle.beam

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.JavaExec

class BeamPlugin implements Plugin<Project> {

    public static final String TASK_GROUP = "beam"

    public static final String RUN_TASK_NAME = "runPipeline"
    public static final String RUN_TASK_DESCRIPTION = "Executes Beam pipeline."

    @Override
    void apply(Project project) {

        def extension = project.extensions.create(BeamExtension.NAME, BeamExtension)

        project.getPluginManager().apply(JavaPlugin.class)

        project.tasks.create([name: RUN_TASK_NAME, type: JavaExec, group: TASK_GROUP, description: RUN_TASK_DESCRIPTION]) {
            classpath = project.sourceSets.main.runtimeClasspath
            main = extension.main

            extension.parameters['runner'] = extension.runner.name
            args = extension.parameters.collect { k, v -> "--$k:$v" }

            jvmArgs = extension.jvmArgs
            environment = extension.environment
        }

        project.dependencies.add('compile', "org.apache.beam:beam-sdks-java-core:${extension.version}")
        project.dependencies.add('runtime', "org.apache.beam:${extension.runner.artifact}:${extension.version}")

    }
}