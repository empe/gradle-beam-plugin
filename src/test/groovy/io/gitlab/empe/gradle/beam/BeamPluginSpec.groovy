package io.gitlab.empe.gradle.beam

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.SourceSet
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification


class BeamPluginSpec extends Specification {

    private final BeamPlugin plugin = new BeamPlugin()

    @Rule final TemporaryFolder testProjectDir = new TemporaryFolder()
    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
    }



    def "should auto apply java plugin"() {

        given:
        def project = initProject()

        when:
        plugin.apply(project)

        then:
        project.plugins.hasPlugin(JavaPlugin.class)
    }

    def "project should have runPipeline task with runtime classpath"() {

        given:
        def project = initProject()

        when:
        plugin.apply(project)
        project.repositories { jcenter() }

        then:
        def task = project.tasks[BeamPlugin.RUN_TASK_NAME]
        task instanceof JavaExec
        task.group == BeamPlugin.TASK_GROUP
        task.classpath == project.sourceSets[SourceSet.MAIN_SOURCE_SET_NAME].runtimeClasspath
    }

    def "compile classpath should contain beam SDK"() {
        given:
        def project = initProject()

        when:
        plugin.apply(project)
        project.repositories { jcenter() }

        then:
        assert getDependency(project, "beam-sdks-java-core", "compileClasspath")
    }

    def "runtime classpath should contain DEFAULT runner when none specified"() {
        given:
        def project = initProject()
        project.repositories { jcenter() }

        when:
        plugin.apply(project)

        then:
        assert getDependency(project, "beam-runners-direct-java", "runtimeClasspath")
    }

    private Project initProject() {
        return ProjectBuilder.builder()
                .withProjectDir(testProjectDir.root)
                .withName(testProjectDir.root.name)
                .build()
    }

    private static File getDependency(Project project, String name, String classpath){
        def filtered = project.sourceSets[SourceSet.MAIN_SOURCE_SET_NAME]."$classpath".filter {
            File f -> f.name.contains(name)
        }

        return filtered.isEmpty() ? null : filtered.getSingleFile()
    }

}